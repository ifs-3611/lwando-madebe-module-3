// ignore_for_file: prefer_const_constructors

import 'package:app_develop/login.dart';
import 'package:flutter/material.dart';


class MyProfile extends StatefulWidget {
  const MyProfile({Key? key}) : super(key: key);

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return Container(
       decoration: const BoxDecoration(
        image: DecorationImage( 
          image: AssetImage('images/register.png'), fit: BoxFit.cover)),
           child: Scaffold(
          backgroundColor: Colors.transparent, 
          body: Stack(
            children: [
                // ignore: duplicate_ignore
                Container(  
                padding: EdgeInsets.only(left: 30, top: 110),
              
                child: Text (
                  'Edit\nYour Profile',
                  style: TextStyle(color: Colors.black, fontSize: 30),
              ),
              ),
               Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.5, right: 35, left: 35),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Name',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Email',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                 SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Password',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    color: Colors.blueGrey,
                    onPressed: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => MyLogin()),);
                    }, 
                    child: Text('Save'),
                    ),
                ],
              )
               ),
            ],
       ),
     ),
    );
  }
}