// ignore_for_file: deprecated_member_use, prefer_const_constructors

import 'package:app_develop/login.dart';
import 'package:flutter/material.dart';

class MyRegister extends StatefulWidget {
  const MyRegister({Key? key}) : super(key: key);

  @override
  State<MyRegister> createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegister> {
  @override
  Widget build(BuildContext context) {
         return Container(
       decoration: const BoxDecoration(
        image: DecorationImage( 
          image: AssetImage('images/register.png'), fit: BoxFit.cover)),
       child: Scaffold(
          backgroundColor: Colors.transparent, 
          body: Stack(
            children: [
                Container(  
                padding: EdgeInsets.only(left: 30, top: 110),
                child: Text (
                  'Create\nYour account',
                  style: TextStyle(color: Colors.black, fontSize: 30),
              ),
              ),
               Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.5, right: 35, left: 35),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Display name',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Email',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                 SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Password',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      TextButton(
                        onPressed: () { 
                          Navigator.push(context, MaterialPageRoute(builder: (context) => MyLogin()),);
                        }, 
                        child: Text(
                          'Sign in', 
                          style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        color: Colors.blueGrey,
                        ),
                        ) )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    color: Colors.blueGrey,
                    onPressed: () {}, 
                    child: Text('Register'),
                    ),
                ],
              )
            )
          ]
         // ignore: avoid_unnecessary_containers  
          ),
       ),
     );
     
  }
}